package com.wecamviewer.data;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mihail on 8/14/2016.
 */
public class VideoDataGroup {
    private List<VideoData> videoDataList = new ArrayList<>();
    private LocalDate date = LocalDate.now();


    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public List<VideoData> getVideoDataList() {
        return videoDataList;
    }

    public void setVideoDataList(List<VideoData> videoDataList) {
        this.videoDataList = videoDataList;
    }
}
