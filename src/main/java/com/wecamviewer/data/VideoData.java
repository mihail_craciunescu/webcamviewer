package com.wecamviewer.data;

import com.wecamviewer.util.DateUtil;

import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Created by Mihail on 8/13/2016.
 */
public class VideoData {

    private String filename;
    private LocalDateTime dateTime;
    private String url;
    private long size;

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public String getTitle(){
        DateTimeFormatter dayFmt = DateTimeFormatter.ofPattern("EEE");
        return dayFmt.format(dateTime) + " " + DateUtil.formatTime(dateTime.toLocalTime());
    }
    public String getSizeAsString(){
        if(size <= 0) return "0";
        final String[] units = new String[] { "B", "kB", "MB", "GB", "TB" };
        int digitGroups = (int) (Math.log10(size)/Math.log10(1024));
        return new DecimalFormat("#,##0.#").format(size/Math.pow(1024, digitGroups)) + " " + units[digitGroups];
    }
}
