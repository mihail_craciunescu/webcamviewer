package com.wecamviewer.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

/**
 * Created by Mihail on 8/13/2016.
 */
@RestController
public class UserController extends AbstractController{
    @RequestMapping("/user")
    public Principal user(Principal principal) {
        return principal;
    }
}
