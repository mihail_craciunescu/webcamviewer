package com.wecamviewer.controller;

import com.wecamviewer.data.VideoData;
import com.wecamviewer.form.RangeForm;
import com.wecamviewer.videoprovider.DlinkS3VideoProvider;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mihail on 7/19/2016.
 */
@Controller
public class StartController extends HeaderFooterController {

    private final static Logger LOG = Logger.getLogger(StartController.class);

    @Autowired
    private DlinkS3VideoProvider dlinkS3VideoProvider;

    @RequestMapping("/")
    public String start( Model model) {
        List<VideoData> videosList = new ArrayList<>();
        if (isAuthorized()){
           videosList = dlinkS3VideoProvider.getTwoDaysVideos();
        }else{
            videosList.add(dlinkS3VideoProvider.getDemoVideo());
        }
        model.addAttribute("videoList", videosList);
        if (CollectionUtils.isNotEmpty(videosList)) {
            model.addAttribute("startVideo", videosList.get(0));
        }
        model.addAttribute("rangeForm", new RangeForm());
        return "start";
    }

    @RequestMapping(value = "/" , method = RequestMethod.POST)
    public String fetchVideos(@ModelAttribute RangeForm rangeForm, Model model) {
        if (rangeForm.isEmpty() || !isAuthorized()){
            return start(model);
        }
        List<VideoData> videosList = dlinkS3VideoProvider.getVideos(rangeForm.getStartDate(),rangeForm.getEndDate());
        model.addAttribute("videoList", videosList);
        model.addAttribute("startVideo", videosList.get(0));
        return "start";
    }

    @RequestMapping("/browse")
    public String browse( Model model) {
        List<VideoData> videosList = new ArrayList<>();
        if (isAuthorized()){
           videosList =  dlinkS3VideoProvider.getTwoDaysVideos();
        }else{
            videosList.add(dlinkS3VideoProvider.getDemoVideo());
        }
        model.addAttribute("videoList", videosList);
        model.addAttribute("rangeForm", new RangeForm());
        return "browse";
    }

    @RequestMapping(value = "/browse" , method = RequestMethod.POST)
    public String fetchVideosBrowse(@ModelAttribute RangeForm rangeForm, Model model) {
        if (rangeForm.isEmpty() || !isAuthorized()){
            return browse(model);
        }
        List<VideoData> videosList = dlinkS3VideoProvider.getVideos(rangeForm.getStartDate(),rangeForm.getEndDate());
        model.addAttribute("videoList", videosList);
        return "browse";
    }

    @RequestMapping("/authorize")
    public String authorize( Model model) {
        if (!model.containsAttribute("authorizedUser")) {
            model.addAttribute("authorizedUser", "");
        }
        model.addAttribute("authorizedUsers", authorizedUsers);
        return "authorize";
    }

    @RequestMapping(value = "/authUser" , method = RequestMethod.GET)
    public String authorizeUser(@RequestParam String userId, Model model,Principal principal){
        LOG.warn(String.format("[AUTHORIZE] User %s authorized by %s",userId,principal.getName()));
        if (StringUtils.isNotBlank(userId) && isAuthorized() && authorizedUsers.contains(principal.getName()) && authorizedUsers.size() <1000){
            authorizedUsers.add(userId.trim());
            model.addAttribute("authorizedUser",userId.trim());
            return authorize(model);
        }
        throw new AccessDeniedException(String.format("User %s cannot be authorized by %s!", userId, principal.getName()));
    }

}
