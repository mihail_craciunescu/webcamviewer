package com.wecamviewer.controller;

import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.security.Principal;
import java.util.Map;

/**
 * Created by Mihail on 8/15/2016.
 */
public abstract class HeaderFooterController extends AbstractController {

    @ModelAttribute("userDisplayName")
    public String getUserDisplayName(Principal principal){
        if (isAuthorized() && authorizedUsers.contains(principal.getName())) {
            if (principal instanceof OAuth2Authentication){
                OAuth2Authentication oAuth2Authentication = (OAuth2Authentication) principal;
                Authentication userAuthentication = oAuth2Authentication.getUserAuthentication();
                return  parseUserFullName(userAuthentication);
            }
            return principal.getName();
        } else if (isAuthorized()){
            throw new AccessDeniedException(String.format("User %s not authorized!", principal.getName()));
        } else{
            return "anonymous";
        }
    }

    @SuppressWarnings("unchecked")
    private String parseUserFullName(Authentication authentication){
        if (authentication.getDetails() instanceof Map) {
            Map<String, Object> map = (Map) authentication.getDetails();
            Object userName = map.get("name");
            if(userName != null){
                return  userName.toString();
            }
        }
        return "unknown";
    }

}
