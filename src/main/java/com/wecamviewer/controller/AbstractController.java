package com.wecamviewer.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.Principal;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by Mihail on 8/13/2016.
 */
public class AbstractController {

    @Autowired
    protected HttpServletResponse httpResponse;

    @Autowired
    protected HttpServletRequest httpRequest;

    @Value("#{'${security.authorizedUsers}'.split(',')}")
    protected Set<String> authorizedUsers;

    @ModelAttribute("authorized")
    public boolean isAuthorized(){
        return httpRequest.isUserInRole("ROLE_USER");
    }

}
