package com.wecamviewer.videoprovider;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.HttpMethod;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.wecamviewer.util.DateUtil;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.net.URL;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Mihail on 8/13/2016.
 */
@Component
public class S3Client {

    @Value("${aws.s3client.accessKey}")
    private String accessKey;
    @Value("${aws.s3client.secretKey}")
    private String secretKey;
    @Value("${aws.s3client.bucketName}")
    private String bucketName;
    @Value("${aws.s3client.expirationTimeInMinutes}")
    private int expirationTimeInMinutes = 10;

    private AmazonS3Client amazonS3Client;


    public String generatePreSignedURL(String objectKey){
        LocalDateTime expiration = LocalDateTime.now().plusMinutes(expirationTimeInMinutes);
        GeneratePresignedUrlRequest generatePresignedUrlRequest = new GeneratePresignedUrlRequest(bucketName, objectKey);
        generatePresignedUrlRequest.setMethod(HttpMethod.GET); // Default.
        generatePresignedUrlRequest.setExpiration(DateUtil.transformDate(expiration));
        URL signedUrl = getAmazonS3Client().generatePresignedUrl(generatePresignedUrlRequest);
        return signedUrl.toString();
    }


    public Map<String, Long> listDateVideos(LocalDate date) {
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyyMMdd");
        String searchPrefix = dateFormatter.format(date);
        ObjectListing objects = getAmazonS3Client().listObjects(bucketName,searchPrefix);

        Map<String, Long> resultMap = new HashMap<>();
        do {
            objects.getObjectSummaries().stream().filter(S3Client.this::testMP4Video).forEach(
                    o -> resultMap.put(o.getKey(),o.getSize())
            );
            objects = getAmazonS3Client().listNextBatchOfObjects(objects);
        } while (objects.isTruncated());

        return resultMap;
    }


    private boolean testMP4Video(S3ObjectSummary objectSummary){
        return "mp4".equalsIgnoreCase(FilenameUtils.getExtension(objectSummary.getKey()));
    }

    public Map<String, Long> listVideos(LocalDate start, LocalDate end) {
        Map<String, Long> resultMap = new HashMap<>();

        while (!start.isAfter(end)){
            resultMap.putAll(listDateVideos(start));
            //limit result
            if (resultMap.size() > 200){
                break;
            }
            start = start.plusDays(1);
        }
        return resultMap;
    }

    protected AmazonS3Client getAmazonS3Client() {
        if (amazonS3Client == null){
            AWSCredentials credentials = new BasicAWSCredentials(accessKey, secretKey);
            ClientConfiguration clientConfig = new ClientConfiguration();
            amazonS3Client = new AmazonS3Client(credentials, clientConfig);
        }
        return amazonS3Client;
    }
}
