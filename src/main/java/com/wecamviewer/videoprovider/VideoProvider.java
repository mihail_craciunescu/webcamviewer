package com.wecamviewer.videoprovider;

import com.wecamviewer.data.VideoData;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by Mihail on 8/14/2016.
 */
public interface VideoProvider {

    List<VideoData> getTwoDaysVideos();
    List<VideoData> getVideos(LocalDate start, LocalDate end);

    default VideoData getDemoVideo(){
        VideoData data = new VideoData();
        data.setFilename("big_buck_bunny.mp4");
        data.setDateTime(LocalDateTime.MIN);
        data.setUrl("http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4");
        data.setSize(5510872);
        return  data;
    }
 }
