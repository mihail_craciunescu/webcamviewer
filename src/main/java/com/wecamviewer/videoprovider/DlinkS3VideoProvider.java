package com.wecamviewer.videoprovider;

import com.wecamviewer.data.VideoData;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * Created by Mihail on 8/13/2016.
 */
@Component
public class DlinkS3VideoProvider implements VideoProvider{
    @Autowired
    private S3Client s3client;

    @Override
    public List<VideoData> getTwoDaysVideos() {
        return getVideos(LocalDate.now().minusDays(1),LocalDate.now());
    }

    private List<VideoData> buildVideoDataList(Map<String, Long> last100Video) {
        List<VideoData> resultList = new ArrayList<>();
        for (Map.Entry<String,Long>entry : last100Video.entrySet()){
            VideoData video = buildVideoData(entry);
            resultList.add(video);
        }
        return resultList;
    }

    private VideoData buildVideoData(Map.Entry<String, Long> entry) {
        String key = entry.getKey();
        VideoData video = new VideoData();
        video.setFilename(extractFilename(key));
        video.setDateTime(extractDateTime(key));
        video.setSize(entry.getValue());
        video.setUrl(s3client.generatePreSignedURL(key));
        return video;
    }

    private LocalDateTime extractDateTime(String key) {
        DateTimeFormatter datetimeFormatter = DateTimeFormatter.ofPattern("yyyyMMdd_HHmm");
        String name = FilenameUtils.getName(key);
        return LocalDateTime.from(datetimeFormatter.parse(name.substring(0,13)));
    }

    private String extractFilename(String key) {
        String filename = FilenameUtils.getName(key);
        return filename;
    }

    @Override
    public List<VideoData> getVideos(LocalDate start, LocalDate end) {
        Map<String,Long> videos = s3client.listVideos(start,end);
        List<VideoData> resultList = buildVideoDataList(videos);
        Collections.sort(resultList, Comparator.comparing(VideoData::getDateTime));
        Collections.reverse(resultList);
        return resultList;
    }

    @Override
    public VideoData getDemoVideo(){
        Map.Entry<String,Long> demoEntry = new AbstractMap.SimpleEntry<>("20160729_185400D.mp4",12484608l);
        return buildVideoData(demoEntry);
    }
}
