package com.wecamviewer.security;

import org.springframework.boot.autoconfigure.security.oauth2.client.EnableOAuth2Sso;
import org.springframework.boot.autoconfigure.security.oauth2.client.OAuth2SsoDefaultConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.csrf.CsrfTokenRepository;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

/**
 * Created by Mihail on 8/15/2016.
 */

@EnableOAuth2Sso
@Configuration
public class WebMVCSecurity  extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
        .antMatcher("/**").authorizeRequests()
        .antMatchers("/","/browse", "/login**", "/js/**", "/css/**")
        .permitAll().anyRequest().authenticated()
        .and().logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout")).logoutSuccessUrl("/").permitAll()
        .and().csrf().csrfTokenRepository(getHttpSessionCsrfTokenRepository());
    }


    @Bean
    public CsrfTokenRepository getHttpSessionCsrfTokenRepository() {
        return new HttpSessionCsrfTokenRepository();
    }
}
