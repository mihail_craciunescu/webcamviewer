package com.wecamviewer.form;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * Created by Mihail on 8/14/2016.
 */
public class RangeForm {
    public static final String MM_DD_YYYY_PATTERN = "MM/dd/yyyy";
    private DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(MM_DD_YYYY_PATTERN);



    private String start;
    private String end;

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public LocalDate getStartDate() {
        LocalDate startDate = LocalDate.from(dateFormatter.parse(start));
        return  startDate;
    }

    public LocalDate getEndDate() {
        LocalDate endDate = LocalDate.from(dateFormatter.parse(end));
        return endDate;
    }

    public boolean isEmpty() {
        return start == null || start.isEmpty() || end == null || end.isEmpty();
    }
}
