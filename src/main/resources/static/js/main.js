/*
*
*/
$(".clockpicker").clockpicker({
    placement: 'right',
    align: 'top',
    autoclose: true
});

/*
* Configuration docs: https://eternicode.github.io/bootstrap-datepicker/
*/
$('#daterangepicker').datepicker({
    daysOfWeekHighlighted: "0",
    autoclose: true,
    todayHighlight: true
});



jQuery(document).ready(function($) {

	});

function loadVideo(panel){
    var videoUrl = $(panel).find(".url").text();
    console.log("[debug] loading video url: " + videoUrl);
    $('#mainPlayer source').attr('src', videoUrl);
    var videoObj = $("#mainPlayer").get(0);
    videoObj.load();
    videoObj.play();
    return true;
}

function filterVideos(){
    var start = $("#daterangepicker input[name='start']").val();
    var end = $("#daterangepicker input[name='end']").val();
    var startEndDate = start + " - "  +end;
    console.log("[debug] filtering videos: " + startEndDate);
    $("#fetch-form").submit();
    return false;
}
function authorize(){
     $("#authorizeForm").submit();
    return false;
}


String.prototype.toHHMM = function () {
    var hVal = parseFloat(this); // don't forget the second param
    var hours   = Math.floor(hVal);
    var minutes = Math.floor((hVal - hours) * 60);

    if (hours   < 10) {hours   = "0"+hours;}
    if (minutes < 10) {minutes = "0"+minutes;}
    return hours+"h "+minutes+" min";
}